import {
  ORDER_FAILURE,
  ORDER_REQUEST,
  ORDER_SUCCESS, TOGGLE_MODAL
} from "../actions/actionTypes";

const initialState = {
    result: [],
    loading: false,
    error: null,
    ordered: false,
    modalVisible: false,
};

const reducerDishes = (state = initialState, action) => {
    switch (action.type) {
        case ORDER_REQUEST:
            return {...state, loading: true};
        case ORDER_SUCCESS:
            return {...state, loading: false, result: action.result};
        case ORDER_FAILURE:
            return {...state, loading: false, error: action.error};
        case TOGGLE_MODAL:
            return {...state, modalVisible: !state.modalVisible};
        default:
            return state;
    }
};

export default reducerDishes;