import axios from '../../axios-orders';
import {ORDER_FAILURE, ORDER_REQUEST, ORDER_SUCCESS, TOGGLE_MODAL} from "./actionTypes";
import {initDishes} from "./actionDishes";

export const orderRequest = () => ({type: ORDER_REQUEST});
export const orderSuccess = result => ({type: ORDER_SUCCESS, result});
export const orderFailure = error => ({type: ORDER_FAILURE, error});
export const toggleModal = () => ({type: TOGGLE_MODAL});

export const createList = () => {
    return dispatch => {
        dispatch(orderRequest());
        return axios.get('pics.json').then(response => {
                dispatch(orderSuccess(response.data.data));
            },
            error => dispatch(orderFailure(error))
        );
    }
};
export const createOrder = (orderData, history) => {
    return dispatch => {
        dispatch(orderRequest());
        axios.post('order.json', orderData).then(
            response => {
                dispatch(createList());
                dispatch(initDishes());
                history.push('/');
            },
            error => dispatch(orderFailure(error))
        );
    }
};
export const deleteOrder = (orderData) => {
    return dispatch => {
        dispatch(orderRequest());
      axios.delete('dishes/' + orderData + '.json').then(
        response => {
          dispatch(createList());
        },
        error => dispatch(orderFailure(error))
        );
    }
};