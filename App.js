import React from 'react';
import {
  StyleSheet, Text, View, Button,
  TouchableOpacity, FlatList, Image, Modal, ScrollView
} from 'react-native';


import {applyMiddleware, combineReducers, createStore} from "redux";
import thunkMiddleware from 'redux-thunk';
import {connect, Provider} from 'react-redux';
import reducerDishes from "./store/reducers/reducerDishes";
import {createList, toggleModal} from "./store/actions/actionOrder";
import reducerOrders from "./store/reducers/reducerOrders";




class App extends React.Component {

  componentDidMount() {
    this.props.createList();
  }

  render() {
      console.log(this.props.result.data.after);
      return  (
        <View style={styles.container}>
          <Text>Hellow world vwvwvwvwvwv</Text>
          <View style={styles.flatContainer}>
              <FlatList
                  data={Object.keys(this.props.result).map((data) => (this.props.result[data]))}
                  renderItem={({item}) => {
                      return <View style={styles.Cart}>
                          <Image
                              source={{uri: item.data.thumbnail}}
                              style={{width: 60, height: 60, marginRight: 10}}
                          />
                          <Text style={{fontSize: 20,}}>{item.data.title}</Text>
                      </View>
                  }}
              />
              {/*<ScrollView>*/}
                  {/*{this.props.result.map((item,key)=>{*/}
                      {/*return <View key={item.data.id}>*/}
                          {/*<Image*/}
                              {/*source={{uri: item.data.thumbnail}}*/}
                              {/*style={{width: 40, height: 40, marginRight: 10}}*/}
                          {/*/>*/}
                          {/*<Text style={{fontSize: 20,}}>{item.data.title}</Text>*/}
                            {/*</View>*/}
                  {/*})}*/}

              {/*</ScrollView>*/}
          </View>
        </View>
    );
  }
}



const mapStateToProps = state => ({
  modalVisible: state.dish.modalVisible,
  result: state.ord.result,
  orderDishes: state.ord.orderDishes,
  totalPrice: state.ord.totalPrice,
  loading: state.ord.loading,
  delivery: state.ord.delivery,

});

const mapDispatchToProps = dispatch => ({
  toggleModal: () => dispatch(toggleModal()),
  createList: () => dispatch(createList()),

});

const AppConnected = connect(mapStateToProps, mapDispatchToProps)(App);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
    paddingHorizontal: 10,
    paddingTop: 30
  },
  flatContainer: {
    flex: 8,
  },
  Cart: {
    padding: 10,
    marginTop: 10,
    height: 60,
    backgroundColor: '#E6E6FA',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textContainer: {
    flexDirection: 'row',
    borderBottomColor: '#4B0082',
    borderBottomWidth: 3,
  },
  priceContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    paddingBottom: 20,
    borderTopColor: '#4B0082',
    borderTopWidth: 3,
  }
});

const rootReducer = combineReducers({
  ord: reducerOrders,
  dish: reducerDishes,
});

const store = createStore(
    rootReducer,
    applyMiddleware(thunkMiddleware)
);

const index = () => (
    <Provider store={store}>
      <AppConnected />
    </Provider>
);


export default index;